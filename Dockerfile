#
# Latest redis
#
FROM iqhive/gentoo-patched
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>
LABEL description="Latest patched gentoo image with redis"

RUN emerge-webrsync; echo "dev-db/redis tcmalloc -jemalloc" > /etc/portage/package.use/redis; emerge -v dev-db/redis; rm -Rf /usr/portage/*

RUN chown redis /usr/sbin/redis-server; chmod u+s /usr/sbin/redis-server; sed -i 's/logfile \/var\/log\/redis\/redis.log/#logfile none.log/g' /etc/redis.conf
# ; sed -i 's/dbfilename dump.rdb/dbfilename \/var\/lib\/redis\/dump.rdb/g' /etc/redis.conf

COPY export/ /

VOLUME /var/lib/redis

EXPOSE 6379

# ENTRYPOINT /usr/sbin/redis-server /etc/redis.conf

